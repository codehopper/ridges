(ns nl.codehopper.ridges.signatures)

(def signatures
  ;; see https://en.wikipedia.org/wiki/List_of_file_signatures
  [{:signature ["89" "50" "4E" "47" "0D" "0A" "1A" "0A"]
    :extension ".png"
    :mime-type "image/png"}
   {:signature ["25" "50" "44" "46" "2D"]
    :extension ".pdf"
    :mime-type "application/pdf"}
   {:signature ["FF" "D8" "FF"]
    :extension ".jpg"
    :mime-type "image/jpeg"}
   {:signature ["42" "4D"]
    :extension ".bmp"
    :mime-type "image/bmp"}
   {:signature ["49" "49" "2A" "00"]
    :extension ".tiff"
    :mime-type "image/tiff"}
   {:signature ["4D" "4D" "00" "2A"]
    :extension ".tiff"
    :mime-type "image/tiff"}
   {:signature ["00" "00" "01" "00"]
    :extension ".ico"
    :mime-type "image/x-icon"}
   {:signature ["1F" "9D"]
    :extension ".tar"
    :mime-type "application/x-tar"}])
