(ns nl.codehopper.ridges
  (:require  #?(:clj [clojure.java.io :as io])
             #?(:cljs [clojure.string :as str])
             [nl.codehopper.ridges.signatures :as sigs]))

(defprotocol BytesMatch
  (matches?  [this signature]))

#?(:clj (extend-protocol BytesMatch

          (Class/forName "[B")
          (matches? [this magic-bytes]
            (let [hex-bytes  (fn [n bytes]
                               (mapv #(format "%02X" %)
                                     (take n bytes)))
                  this-bytes (hex-bytes (count magic-bytes) this)]
              (= magic-bytes this-bytes)))

          java.io.File
          (matches? [this magic-bytes]
            (let [size (count magic-bytes)
                  fis  (java.io.FileInputStream. this)
                  buf  (byte-array size)
                  _    (.read fis buf)]
              (matches? buf magic-bytes)))))

#?(:cljs (extend-protocol BytesMatch

           js/Buffer
           (matches? [this magic-bytes]
             (let [size       (count magic-bytes)
                   this-bytes (->> (.toString this "hex")
                                   (str/upper-case)
                                   (partition 2)
                                   (map #(apply str %))
                                   (take size))]
               (= magic-bytes this-bytes)))

           js/Uint8Array
           (matches? [this magic-bytes]
             (let [size       (count magic-bytes)
                   as-hex     (fn [num]
                                (let [hex    (.toString num 16)
                                      padded (.slice (str 00 hex) -2)]
                                  (str/upper-case padded)))
                   this-bytes (->> this
                                   array-seq
                                   (take size)
                                   (map as-hex))]
               (= magic-bytes this-bytes)))))

(defn mime
  "Finds the MIME type of the provided thing, if it satisfies BytesMatch"
  [thing]
  (if (satisfies? BytesMatch thing)
    (loop [candidates sigs/signatures]
      (when (seq candidates)
        (let [[candidate & candidates] candidates]
          (if (matches? thing (:signature candidate))
            (:mime-type candidate)
            (recur candidates)))))))
