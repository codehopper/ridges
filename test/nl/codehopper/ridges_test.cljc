(ns nl.codehopper.ridges-test
  (:require [nl.codehopper.ridges :as sut]
            #?@(:clj [[clojure.test :as t]
                      [clojure.java.io :as io]]
                :cljs [[cljs.test :as t :include-macros true]
                       [cljs-node-io.core :as io]
                       [cljs-node-io.fs :as fs]])))

(def magic-png ["89" "50" "4E" "47" "0D" "0A" "1A" "0A"])
(def magic-pdf ["25" "50" "44" "46" "2D"])

#?(:clj (defn resource-bytes [path]
          (let [resource (io/resource path)]
            (with-open [out (java.io.ByteArrayOutputStream.)]
              (io/copy (-> resource
                           (io/input-stream))
                       out)
              (.toByteArray out))))

   :cljs (defn resource-bytes [path]
           (let [test-root     "test-resources"
                 resource-path (fs/resolve-path "." test-root path)]
             (fs/readFile resource-path ""))))

(t/deftest magic-bytes-test
  (t/testing "Given the magic-bytes signature for PNG"
    (t/testing "and a yet unknown PNG byte stream"
      (let [unknown-png (resource-bytes "logo.png")]
        (t/testing "then the signature matches"
          (t/is (sut/matches? unknown-png magic-png)))))))

#?(:clj (t/deftest file-magic-bytes-test
          (t/testing "Given the magic-bytes signature for PNG"
            (t/testing "And a yet unknown PNG file"
              (let [unknown-file (-> "logo.png"
                                     io/resource
                                     io/file)]
                (t/is (sut/matches? unknown-file magic-png))
                (t/is (not (sut/matches? unknown-file magic-pdf)))))))
   :cljs (t/deftest uint-magic-bytes-test
           (t/testing "Given the magic-bytes signature for PNG"
             (t/testing "And a yet unknown PNG file"
               (let [buffer (resource-bytes "logo.png")]
                 (t/testing "When I convert it to a Uint8Array"
                   (let [array (js/Uint8Array. buffer)]
                     (t/testing "Then the signature still matches"
                       (t/is (sut/matches? array magic-png))))))))))

(t/deftest mime-test
  (t/testing "Given a PNG and a PDF"
    (let [things (map #?(:clj (comp io/file io/resource)
                         :cljs resource-bytes)
                      ["logo.png" "dummy.pdf"])]
      (t/testing "Then the mimetypes are the expected ones"
        (t/is (= ["image/png" "application/pdf"]
                 (map sut/mime things)))))))
