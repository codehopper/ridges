_____
I had gained the summit of a commanding ridge, and, looking round with astonishing delight, beheld the ample plains, the beauteous tracts below.

-- Daniel Boone
_____

image::doc/logo.png[Ridges logo, 200, 150]

= ridges

A Clojure[Script] no-deps library that guesses the MIME type of a file or byte sequence from its
link:https://en.wikipedia.org/wiki/List_of_file_signatures[magic bytes].

= Usage

TBG

= License

Copyright © 2019 Code Hopper

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
